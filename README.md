# check_mysql_conf

### 脚本介绍
比较两个mysql实例的配置是否一致，支持比较配置文件，也支持比较系统变量的值。

### 脚本使用
##### 对比系统变量
```
#脚本的opt变量必须是system
#mysql连接配置需要用户自己定义
#配置好直接执行如下命令
#效果如下图
root /data/git/script >> bash check_mysql_conf_diff.sh
```
![](http://52wiki.oss-cn-beijing.aliyuncs.com/doc/2f1fe9ba224dd0e7cb90fc436704a876a0a25c9a.png)


##### 对比配置文件
```
#脚本的opt变量必须是conf
#配置好直接执行如下命令，其中配置文件必须存在，建议是绝对路径，效果如下图
#效果如下图
root /data/git/script >> bash check_mysql_conf_diff.sh /data/mysql/etc/3306/my.cnf  /data/mysql/etc/3311/my.cnf
```

![](http://52wiki.oss-cn-beijing.aliyuncs.com/doc/3d92fe70f19d8aa13a23d612ac7a23fdc8c3ced8.png)

