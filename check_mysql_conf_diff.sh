#!/bin/bash
# File Name   : check_mysql_conf_diff.sh
# Author      : moshan
# Mail        : mo_shan@yeah.net
# Created Time: 2019-12-19 15:18:40
# Function    : 比较两个mysql实例的配置是否一致，支持比较配置文件，也支持比较系统变量的值
#########################################################################
opt="system"  #conf|system  conf表示对比配置文件，system表示对比系统变量
skip_array=(gtid_purged gtid_executed relay_log_basename port)   #表示跳过比较哪些变量
#如果是system的话，需要配置下面与mysql连接相关的参数
mysql_host1="127.0.0.1"
mysql_user1="test"
mysql_pass1="test"
mysql_port1="3306"
mysql_host2="127.0.0.1"
mysql_user2="test"
mysql_pass2="test"
mysql_port2="3311"
mysql_path="/data/mysql/base/bin/mysql"
function f_logging()
{
	log_mode="${1}"
	log_info="${2}"
	log_enter="${3}"
	exit_mark="${4}"
	enter_opt=""
	if [ "${log_mode}x" == "WARNx" ]
	then
		echo -e "\033[33m"
	elif [ "${log_mode}x" == "ERRORx" ]
	then
		echo -e "\033[31m"
	else
		echo -en "\033[32m"
	fi
	if [ "${log_enter}x" == "0x" ]
	then
		log_enter="-n"
	elif [ "${log_enter}x" == "2x" ]
	then
		log_enter="-e"
		enter_opt="\n"
	else
		log_enter="-e"
	fi
	echo ${log_enter} "[$(date "+%F %H:%M:%S")] [${log_mode}] [${localhost_ip}] ${log_info}${enter_opt}"
	echo -en "\033[0m"
	if [ "${log_mode}x" == "ERRORx" -a "${exit_mark}x" != "0x" ]
	then
		exit
	fi
	echo -en "\033[32m"
}
function f_format_print_info()
{
	show_str="${1}"
	if [ "$(grep -cE "^space_str|space_str$|space_strs$" <<< "${show_str}")x" == "1x" ]
	then
		show_str_len=$((${#show_str}-9))
	else
		show_str_len=${#show_str}
	fi
	max_len="${2}"
	tmp_len=$((${max_len}-${show_str_len}))
	for ((var=0;var<${tmp_len};var++))
	do
		if [ "${1}x" == "-x" ]
		then
			show_str="${show_str}-"
		else
			show_str="${show_str}space_str"
		fi
	done
}
function f_enter_str()
{
	var_pos=0
	str_type=(0 0)
	clo=(0 0)
	for var in ${@}
	do
		col_len="${col_array[${var_pos}]}"
		eval str_type[${var_pos}]="${var}"
		f_format_print_info "${str_type[${var_pos}]}" "${col_len}"
		eval clo[${var_pos}]="${show_str}"
		var_pos=$((${var_pos}+1))
	done
	show_info=""
	for ((i=0;i<${#clo[@]};i++))
	do
		if [ "${i}x" == "0x" ]
		then
			if [ "${str_type[0]}x" == "-x" ]
			then
				show_info="${show_info}\033[32m+${clo[${i}]}-+-"
			else
				show_info="\033[32m|\033[0m\033[33m${clo[${i}]}\033[0m\033[32m|\033[0m"
			fi
		elif [ "${i}x" == "$((${#clo[@]}-1))x" ]
		then
			if [ "${str_type[0]}x" == "-x" ]
			then
				show_info="${show_info}${clo[${i}]}-+\033[0m"
			else
				show_info="${show_info}\033[33m ${clo[${i}]}\033[0m\033[32m|\033[0m"
			fi
		else
			if [ "${str_type[0]}x" == "-x" ]
			then
				show_info="${show_info}${clo[${i}]}-+-"
			else
				show_info="${show_info}\033[33m ${clo[${i}]}\033[0m \033[32m|\033[0m"
			fi
		fi
	done
	echo -e "${show_info}"|sed 's/space_str/ /g'|sed 's/:=/ -/g'|sed 's/@@@/ /g'
}
file1="${1}"
file2="${2}"
tmp_file1="/tmp/.tmp1"
tmp_file2="/tmp/.tmp2"
[ ! -d "/tmp" ] && mkdir /tmp
if [ "${#}x" != "2x" -a "${opt}x" == "confx" ]
then
	f_logging "WARN" "bash ${0} file1 file2" "2"
	exit
elif [ ! -f "${file1}" -a "${opt}x" == "confx" ]
then
	f_logging "ERROR" "${file1} : No such file." "2" "1"
elif [ ! -f "${file2}" -a "${opt}x" == "confx" ]
then
	f_logging "ERROR" "${file2} : No such file." "2" "1"
fi
if [ "${opt}x" == "confx" ] 
then
	cat ${file1}|grep -v "^#"|tr -d " "|tr -d "\t" |sed 's/^ //g'|sed -n '/^\[mysqld\]$/,/\[.*\]/p'|grep "=" > ${tmp_file1}
	cat ${file2}|grep -v "^#"|tr -d " "|tr -d "\t" |sed 's/^ //g'|sed -n '/^\[mysqld\]$/,/\[.*\]/p'|grep "=" > ${tmp_file2}
elif [ "${opt}x" == "systemx" ]
then
	file1="${mysql_port1}"
	file2="${mysql_port2}"
	${mysql_path} -u${mysql_user1} -p${mysql_pass1} -P${mysql_port1} -h${mysql_host1} -NBe "select concat(VARIABLE_NAME,'=',VARIABLE_VALUE) from performance_schema.global_variables;" 2>/dev/null |sed 's/ /@@@/g' > ${tmp_file1}
	if [ ! -s "${tmp_file1}" ]
	then
		f_logging "ERROR" "The first mysql node is abnormally connected" "2" "1"
	fi
	${mysql_path} -u${mysql_user2} -p${mysql_pass2} -P${mysql_port2} -h${mysql_host2} -NBe "select concat(VARIABLE_NAME,'=',VARIABLE_VALUE) from performance_schema.global_variables;" 2>/dev/null |sed 's/ /@@@/g' > ${tmp_file2} 
	if [ ! -s "${tmp_file2}" ]
	then
		f_logging "ERROR" "The second mysql node is abnormally connected" "2" "1"
	fi
fi
col_array=(40 50 50)
f_enter_str "-" "-" "-"
f_enter_str "Variablespace_str" "${file1}" "${file2}space_str"
for var in $(cat ${tmp_file1})
do
	var_name=$(awk -F= '{print $1}' <<< "${var}")
	[ "$(echo ${skip_array[@]}|tr " " "\n"|grep -c "^${var_name}$")x" == "1x" ] && continue
	var_var=$(awk -F= '{print $2}' <<< "${var}")
	var_var2=$(grep "^${var_name}=" ${tmp_file2} |awk -F= '{print $2}'|tail -1)
	if [ "${var_var}x" != "${var_var2}x" ]
	then
		[ "${var_var2}x" == "x" ] && var_var2="default"
		f_enter_str "-" "-" "-"
		f_enter_str "${var_name}space_str" "${var_var}" "${var_var2}space_str"
	fi
done
for var in $(cat ${tmp_file2})
do
	var_name=$(awk -F= '{print $1}' <<<"${var}")
	[ "$(echo ${skip_array[@]}|tr " " "\n"|grep -c "^${var_name}$")x" == "1x" ] && continue
	var_var=$(awk -F= '{print $2}' <<<"${var}")
	var_var2=$(grep "^${var_name}=" ${tmp_file1} |awk -F= '{print $2}'|tail -1)
	if [ "${var_var}x" != "${var_var2}x" ]
	then
		[ "${var_var2}x" == "x" ] && var_var2="default" || continue
		f_enter_str "-" "-" "-"
		f_enter_str "${var_name}space_str" "${var_var2}" "${var_var}space_str"
	fi
done
f_enter_str "-" "-" "-"
[ -f "${tmp_file1}" ] && rm -f ${tmp_file1}
[ -f "${tmp_file2}" ] && rm -f ${tmp_file2}
